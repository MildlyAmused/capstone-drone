# CMake generated Testfile for 
# Source directory: /home/react/catkin_ws/src/mocap_optitrack
# Build directory: /home/react/catkin_ws/build/mocap_optitrack
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_mocap_optitrack_roslaunch-check_launch "/home/react/catkin_ws/build/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/kinetic/share/catkin/cmake/test/run_tests.py" "/home/react/catkin_ws/build/test_results/mocap_optitrack/roslaunch-check_launch.xml" "--return-code" "/usr/bin/cmake -E make_directory /home/react/catkin_ws/build/test_results/mocap_optitrack" "/opt/ros/kinetic/share/roslaunch/cmake/../scripts/roslaunch-check -o '/home/react/catkin_ws/build/test_results/mocap_optitrack/roslaunch-check_launch.xml' '/home/react/catkin_ws/src/mocap_optitrack/launch' ")
subdirs(src)
