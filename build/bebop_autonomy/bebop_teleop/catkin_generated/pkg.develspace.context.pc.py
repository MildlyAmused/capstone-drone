# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/react/catkin_ws/src/bebop_autonomy/bebop_teleop/include".split(';') if "/home/react/catkin_ws/src/bebop_autonomy/bebop_teleop/include" != "" else []
PROJECT_CATKIN_DEPENDS = "geometry_msgs;roscpp;std_msgs;sensor_msgs;bebop_msgs;cv_bridge;image_transport".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lbebop_teleop".split(';') if "-lbebop_teleop" != "" else []
PROJECT_NAME = "bebop_teleop"
PROJECT_SPACE_DIR = "/home/react/catkin_ws/devel"
PROJECT_VERSION = "0.1.0"
