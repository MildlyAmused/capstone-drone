# CMake generated Testfile for 
# Source directory: /home/react/catkin_ws/src
# Build directory: /home/react/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(bebop_autonomy/bebop_autonomy)
subdirs(bebop_autonomy/bebop_msgs)
subdirs(bebop_autonomy/bebop_tools)
subdirs(Bebop_ROS_Examples/datalogger)
subdirs(Bebop_ROS_Examples/odometry)
subdirs(bebop_autonomy/bebop_description)
subdirs(pid)
subdirs(Bebop_ROS_Examples/bebop_color_follower)
subdirs(bebop_autonomy/bebop_teleop)
subdirs(Bebop_ROS_Examples/opencv_example)
subdirs(mocap_optitrack)
subdirs(Bebop_ROS_Examples/Bebop_waypoints)
subdirs(Bebop_ROS_Examples/vicon_mimo_bebop)
subdirs(Bebop_ROS_Examples/vicon_wand)
subdirs(bebop_autonomy/bebop_driver)
