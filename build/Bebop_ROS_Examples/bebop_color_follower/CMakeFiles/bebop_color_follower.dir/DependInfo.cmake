# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/bebop_color_follower/src/bebop_color_follower.cpp" "/home/react/catkin_ws/build/Bebop_ROS_Examples/bebop_color_follower/CMakeFiles/bebop_color_follower.dir/src/bebop_color_follower.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"bebop_color_follower\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/bebop_color_follower/include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/include/opencv-3.3.1-dev"
  "/opt/ros/kinetic/include/opencv-3.3.1-dev/opencv"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
