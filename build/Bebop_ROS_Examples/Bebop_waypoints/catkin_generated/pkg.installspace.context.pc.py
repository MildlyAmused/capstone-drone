# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "geometry_msgs;roscpp;sensor_msgs;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lBebop_waypoints".split(';') if "-lBebop_waypoints" != "" else []
PROJECT_NAME = "Bebop_waypoints"
PROJECT_SPACE_DIR = "/home/react/catkin_ws/install"
PROJECT_VERSION = "0.0.0"
