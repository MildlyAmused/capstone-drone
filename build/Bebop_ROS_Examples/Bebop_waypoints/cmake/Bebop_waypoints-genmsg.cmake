# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "Bebop_waypoints: 2 messages, 0 services")

set(MSG_I_FLAGS "-IBebop_waypoints:/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg;-Igeometry_msgs:/opt/ros/kinetic/share/geometry_msgs/cmake/../msg;-Isensor_msgs:/opt/ros/kinetic/share/sensor_msgs/cmake/../msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(Bebop_waypoints_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg" NAME_WE)
add_custom_target(_Bebop_waypoints_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "Bebop_waypoints" "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg" "geometry_msgs/Point:std_msgs/Header:Bebop_waypoints/Waypoint"
)

get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg" NAME_WE)
add_custom_target(_Bebop_waypoints_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "Bebop_waypoints" "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg" "std_msgs/Header:geometry_msgs/Point"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(Bebop_waypoints
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/Bebop_waypoints
)
_generate_msg_cpp(Bebop_waypoints
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/Bebop_waypoints
)

### Generating Services

### Generating Module File
_generate_module_cpp(Bebop_waypoints
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/Bebop_waypoints
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(Bebop_waypoints_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(Bebop_waypoints_generate_messages Bebop_waypoints_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg" NAME_WE)
add_dependencies(Bebop_waypoints_generate_messages_cpp _Bebop_waypoints_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg" NAME_WE)
add_dependencies(Bebop_waypoints_generate_messages_cpp _Bebop_waypoints_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(Bebop_waypoints_gencpp)
add_dependencies(Bebop_waypoints_gencpp Bebop_waypoints_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS Bebop_waypoints_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(Bebop_waypoints
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/Bebop_waypoints
)
_generate_msg_eus(Bebop_waypoints
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/Bebop_waypoints
)

### Generating Services

### Generating Module File
_generate_module_eus(Bebop_waypoints
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/Bebop_waypoints
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(Bebop_waypoints_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(Bebop_waypoints_generate_messages Bebop_waypoints_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg" NAME_WE)
add_dependencies(Bebop_waypoints_generate_messages_eus _Bebop_waypoints_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg" NAME_WE)
add_dependencies(Bebop_waypoints_generate_messages_eus _Bebop_waypoints_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(Bebop_waypoints_geneus)
add_dependencies(Bebop_waypoints_geneus Bebop_waypoints_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS Bebop_waypoints_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(Bebop_waypoints
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/Bebop_waypoints
)
_generate_msg_lisp(Bebop_waypoints
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/Bebop_waypoints
)

### Generating Services

### Generating Module File
_generate_module_lisp(Bebop_waypoints
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/Bebop_waypoints
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(Bebop_waypoints_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(Bebop_waypoints_generate_messages Bebop_waypoints_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg" NAME_WE)
add_dependencies(Bebop_waypoints_generate_messages_lisp _Bebop_waypoints_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg" NAME_WE)
add_dependencies(Bebop_waypoints_generate_messages_lisp _Bebop_waypoints_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(Bebop_waypoints_genlisp)
add_dependencies(Bebop_waypoints_genlisp Bebop_waypoints_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS Bebop_waypoints_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(Bebop_waypoints
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/Bebop_waypoints
)
_generate_msg_nodejs(Bebop_waypoints
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/Bebop_waypoints
)

### Generating Services

### Generating Module File
_generate_module_nodejs(Bebop_waypoints
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/Bebop_waypoints
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(Bebop_waypoints_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(Bebop_waypoints_generate_messages Bebop_waypoints_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg" NAME_WE)
add_dependencies(Bebop_waypoints_generate_messages_nodejs _Bebop_waypoints_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg" NAME_WE)
add_dependencies(Bebop_waypoints_generate_messages_nodejs _Bebop_waypoints_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(Bebop_waypoints_gennodejs)
add_dependencies(Bebop_waypoints_gennodejs Bebop_waypoints_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS Bebop_waypoints_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(Bebop_waypoints
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/Bebop_waypoints
)
_generate_msg_py(Bebop_waypoints
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Point.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/Bebop_waypoints
)

### Generating Services

### Generating Module File
_generate_module_py(Bebop_waypoints
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/Bebop_waypoints
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(Bebop_waypoints_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(Bebop_waypoints_generate_messages Bebop_waypoints_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoints.msg" NAME_WE)
add_dependencies(Bebop_waypoints_generate_messages_py _Bebop_waypoints_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/Bebop_waypoints/msg/Waypoint.msg" NAME_WE)
add_dependencies(Bebop_waypoints_generate_messages_py _Bebop_waypoints_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(Bebop_waypoints_genpy)
add_dependencies(Bebop_waypoints_genpy Bebop_waypoints_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS Bebop_waypoints_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/Bebop_waypoints)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/Bebop_waypoints
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_cpp)
  add_dependencies(Bebop_waypoints_generate_messages_cpp geometry_msgs_generate_messages_cpp)
endif()
if(TARGET sensor_msgs_generate_messages_cpp)
  add_dependencies(Bebop_waypoints_generate_messages_cpp sensor_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(Bebop_waypoints_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/Bebop_waypoints)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/Bebop_waypoints
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_eus)
  add_dependencies(Bebop_waypoints_generate_messages_eus geometry_msgs_generate_messages_eus)
endif()
if(TARGET sensor_msgs_generate_messages_eus)
  add_dependencies(Bebop_waypoints_generate_messages_eus sensor_msgs_generate_messages_eus)
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(Bebop_waypoints_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/Bebop_waypoints)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/Bebop_waypoints
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_lisp)
  add_dependencies(Bebop_waypoints_generate_messages_lisp geometry_msgs_generate_messages_lisp)
endif()
if(TARGET sensor_msgs_generate_messages_lisp)
  add_dependencies(Bebop_waypoints_generate_messages_lisp sensor_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(Bebop_waypoints_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/Bebop_waypoints)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/Bebop_waypoints
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_nodejs)
  add_dependencies(Bebop_waypoints_generate_messages_nodejs geometry_msgs_generate_messages_nodejs)
endif()
if(TARGET sensor_msgs_generate_messages_nodejs)
  add_dependencies(Bebop_waypoints_generate_messages_nodejs sensor_msgs_generate_messages_nodejs)
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(Bebop_waypoints_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/Bebop_waypoints)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/Bebop_waypoints\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/Bebop_waypoints
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_py)
  add_dependencies(Bebop_waypoints_generate_messages_py geometry_msgs_generate_messages_py)
endif()
if(TARGET sensor_msgs_generate_messages_py)
  add_dependencies(Bebop_waypoints_generate_messages_py sensor_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(Bebop_waypoints_generate_messages_py std_msgs_generate_messages_py)
endif()
