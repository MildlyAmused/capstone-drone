set(_CATKIN_CURRENT_PACKAGE "datalogger")
set(datalogger_VERSION "0.0.0")
set(datalogger_MAINTAINER "Alexis Guijarro <totonzx@gmail.com>")
set(datalogger_PACKAGE_FORMAT "1")
set(datalogger_BUILD_DEPENDS "nav_msgs" "roscpp" "std_msgs")
set(datalogger_BUILD_EXPORT_DEPENDS "nav_msgs" "roscpp" "std_msgs")
set(datalogger_BUILDTOOL_DEPENDS "catkin")
set(datalogger_BUILDTOOL_EXPORT_DEPENDS )
set(datalogger_EXEC_DEPENDS "nav_msgs" "roscpp" "std_msgs")
set(datalogger_RUN_DEPENDS "nav_msgs" "roscpp" "std_msgs")
set(datalogger_TEST_DEPENDS )
set(datalogger_DOC_DEPENDS )
set(datalogger_URL_WEBSITE "")
set(datalogger_URL_BUGTRACKER "")
set(datalogger_URL_REPOSITORY "")
set(datalogger_DEPRECATED "")