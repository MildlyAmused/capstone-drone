# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "datalogger: 1 messages, 0 services")

set(MSG_I_FLAGS "-Idatalogger:/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg;-Inav_msgs:/opt/ros/kinetic/share/nav_msgs/cmake/../msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/kinetic/share/geometry_msgs/cmake/../msg;-Iactionlib_msgs:/opt/ros/kinetic/share/actionlib_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(datalogger_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg" NAME_WE)
add_custom_target(_datalogger_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "datalogger" "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg" "std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(datalogger
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/datalogger
)

### Generating Services

### Generating Module File
_generate_module_cpp(datalogger
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/datalogger
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(datalogger_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(datalogger_generate_messages datalogger_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg" NAME_WE)
add_dependencies(datalogger_generate_messages_cpp _datalogger_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(datalogger_gencpp)
add_dependencies(datalogger_gencpp datalogger_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS datalogger_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(datalogger
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/datalogger
)

### Generating Services

### Generating Module File
_generate_module_eus(datalogger
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/datalogger
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(datalogger_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(datalogger_generate_messages datalogger_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg" NAME_WE)
add_dependencies(datalogger_generate_messages_eus _datalogger_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(datalogger_geneus)
add_dependencies(datalogger_geneus datalogger_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS datalogger_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(datalogger
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/datalogger
)

### Generating Services

### Generating Module File
_generate_module_lisp(datalogger
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/datalogger
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(datalogger_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(datalogger_generate_messages datalogger_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg" NAME_WE)
add_dependencies(datalogger_generate_messages_lisp _datalogger_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(datalogger_genlisp)
add_dependencies(datalogger_genlisp datalogger_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS datalogger_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(datalogger
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/datalogger
)

### Generating Services

### Generating Module File
_generate_module_nodejs(datalogger
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/datalogger
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(datalogger_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(datalogger_generate_messages datalogger_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg" NAME_WE)
add_dependencies(datalogger_generate_messages_nodejs _datalogger_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(datalogger_gennodejs)
add_dependencies(datalogger_gennodejs datalogger_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS datalogger_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(datalogger
  "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/datalogger
)

### Generating Services

### Generating Module File
_generate_module_py(datalogger
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/datalogger
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(datalogger_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(datalogger_generate_messages datalogger_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/react/catkin_ws/src/Bebop_ROS_Examples/datalogger/msg/Altitude_datalogger.msg" NAME_WE)
add_dependencies(datalogger_generate_messages_py _datalogger_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(datalogger_genpy)
add_dependencies(datalogger_genpy datalogger_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS datalogger_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/datalogger)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/datalogger
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET nav_msgs_generate_messages_cpp)
  add_dependencies(datalogger_generate_messages_cpp nav_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(datalogger_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/datalogger)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/datalogger
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET nav_msgs_generate_messages_eus)
  add_dependencies(datalogger_generate_messages_eus nav_msgs_generate_messages_eus)
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(datalogger_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/datalogger)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/datalogger
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET nav_msgs_generate_messages_lisp)
  add_dependencies(datalogger_generate_messages_lisp nav_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(datalogger_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/datalogger)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/datalogger
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET nav_msgs_generate_messages_nodejs)
  add_dependencies(datalogger_generate_messages_nodejs nav_msgs_generate_messages_nodejs)
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(datalogger_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/datalogger)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/datalogger\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/datalogger
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET nav_msgs_generate_messages_py)
  add_dependencies(datalogger_generate_messages_py nav_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(datalogger_generate_messages_py std_msgs_generate_messages_py)
endif()
