# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/react/catkin_ws/src/Bebop_ROS_Examples/vicon_mimo_bebop/include".split(';') if "/home/react/catkin_ws/src/Bebop_ROS_Examples/vicon_mimo_bebop/include" != "" else []
PROJECT_CATKIN_DEPENDS = "geometry_msgs;roscpp;sensor_msgs;std_msgs".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "".split(';') if "" != "" else []
PROJECT_NAME = "vicon_mimo_bebop"
PROJECT_SPACE_DIR = "/home/react/catkin_ws/devel"
PROJECT_VERSION = "0.0.0"
