#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define radiansToDegrees (M_PI / 180)
#define FILENAME "test_1.yaml"
#define ZVAL "0.7"
#define HEADING "0.0"
#define TIMEGOAL "5.0"
#define WAYPNUM "1"

int main(int argc, char **argv){
	FILE *fp = fopen(FILENAME, "w");
	double targetLon = strtod(argv[1], NULL);
	double targetLat = strtod(argv[2], NULL);
	double currLon = strtod(argv[3], NULL);
	double currLat = strtod(argv[4], NULL);

	double x = cos(targetLat*radiansToDegrees)*sin(fabs((fabs(currLon)) - 
	(fabs(targetLon)))*radiansToDegrees);
	double y = cos(currLat*radiansToDegrees)*sin(targetLat*radiansToDegrees) - 
	sin(currLat*radiansToDegrees)*cos(targetLat*radiansToDegrees)*
	cos(fabs((fabs(currLon)) - (fabs(targetLon)))*radiansToDegrees);
	double direction = atan2(x, y) / radiansToDegrees;
	if ((targetLon - currLon) < 0) 
		direction = 360 - direction;

	fprintf(fp, "waypoints: %s\n", WAYPNUM);
	fprintf(fp, "X: [%s, %s]\n", argv[3], argv[1]);
	fprintf(fp, "Y: [%s, %s]\n", argv[2], argv[2]);
	fprintf(fp, "Z: [%s]\n", ZVAL);
	fprintf(fp, "Hdg: [%f]\n", direction);
	//fprintf(fp, "Hdg: [0]\n");
	fprintf(fp, "Time_goal: [%s]\n", TIMEGOAL);
	fprintf(fp, "bebop_name: bebop_18\n");
	fclose(fp);
	return 0;
}
