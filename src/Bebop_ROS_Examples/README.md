# Running the Bebop driver:
1. Press the power button on Bebop to start it
2. Once the blue light stops blinking, press the button 3 times. You should hear 2 beeps. After second beep the drone will connect to the local network REACT-5G (ip = 192.168.1.17 or 192.168.1.18)
3. Clone the *bebop_autonomy* project in catkin workspace src directory
4. Make sure that roscore is running on the master node.
5. Start the driver : 
```
roslaunch bebop_driver bebop_node.launch ip:=192.168.1.18 namespace:=bebop_18
```
Note: soft_limit and hard_limit for height can be modified in the bebop_node.launch file.

# Streaming pose data from motion capture system from drone.
1. Make sure that the motion capture system is connected to the REACT-5G network and is streaming on the local ip.
2. Create a rigid body object of the drone (after attaching tracking markers to it) using the motive software and give it a unique id
3. Clone the mocap_optitrack project from REACT gitlab directory in your catkin workspace src directory
4. Edit the *catkin_ws/src/mocap_optitrack/config/mocap.yaml* file to add a new entry for the drone using the unique id.(if not done already)
5. run : 
```
catkin_make
```
6. If you are running ROS master locally, then run: 
```
roslaunch mocap_optitrack mocap.launch
```
7. The rostopic list should show you


#  Bebop_ROS_Examples

This is a compilation of examples ready-to-use for Parrot's Bebop Drone, these examples depends of the following:
  - ROS Kinetic
  - Bebop Autonomy 
  - Vicon Bridge (Not used in REACT Lab)
  - OpenCV
  - ros-kinetic-pid (install this package by cloning from here: http://wiki.ros.org/pid (install and build from source). Don't use apt-get to install)

**Mexican National Robotic Tournament 2018, autonomous drone category's best Color Follower added**

These are the examples in this repository:
## Simple Test

  - *Package:* **bebop_simple_test**
  - *Node:* **bebop_test**
   
  This program allow to the user to take-off, landing and move across the space using the sensors in the drone. There are four test, and can be selected at the beginning of the source code:

- [**0**] SIMPLE TAKE-OFF & LANDING: It performs a quick takeoff and
landing at the same place.
- [**1**] SIMPLE ROUTINE: It performs a actions like elevate, descend,
hover and rotating at certain headings.
- [**2**] COMPLEX ROUTINE: It performs combined actions (elevate/descend
as it rotates at certain heading).
- [**3**] CONTINUOUS ROTATING: It keeps a specified altitude and rotates
infinitely until < Ctrl + C > is pressed and lands.

## Datalogger

  - *Package:* **datalogger**
  - *Node:* **datalogger_node**
   
  This tool capture data from the drone sensors and programs and it exports them into a CSV file:

+ Altitude (Ultrasonic Sensor)
+ Odometry
+ PD Outputs 

The output file can be easily used in many Spreadsheet programs and MATLAB.

## Odometry

  - *Package:* **odometry**
  - *Node:* **odometry**

This tool visualizes the different data acquired by Bebop Autonomy.

## OpenCV Example

  - *Package:* **opencv_example**
  - *Node:* **opencv_example**

This program shows the capability to use the image stream from the
drone and prepare it for its use with OpenCV.

## Vicon Test (not used in REACT lab)

  - *Package:* **vicon_test**
  - *Node:* **vicon_test_node**
   
  This program performs a basic routine with the vicon camera system using waypoints, which are configurable at the code, the drone always will keep its sight to the center of the world, to avoid any unpredictable behavior this last action will not be effectuated until the drone is outside of a deadzone area, also configurable.

****Note: To stop the program and land the drone push down and hold the Button[0] of your joystick, to know which button is, open a new terminal with the rostopic echo /joy command and press the buttons until it change from a state of 0 to 1.***

## Vicon Wand (Not used in REACT lab)

  - *Package:* **vicon_wand**
  - *Node:* **vicon_wand_node**

This program need a second object created in the vicon system as a wand, the drone will try to move to the front of the wand and change its heading to match the face of the wand. **Be aware of your surroundings!**

****Note: To stop the program and land the drone push down and hold the Button[0] of your joystick, to know which button is, open a new terminal with the rostopic echo /joy command and press the buttons until it change from a state of 0 to 1.***

## Vicon Mimo Bebop

  - *Package:* **vicon_mimo_bebop**
  - *Node:* **vicon_mimo_bebop_node**
   
This program requires a second Parrot Bebop 1 registered in the vicon system, refer to the configuration of a drone for the vicon programs. This program in particular, does not interact with the joystick, instead, it listen only the messages of take-off and land that are sent to the master drone (the default namespace bebop of the bebop driver), and send it to the slave drone too.

Any executable that uses Bebop Autonomy and a joystick to manually pilot the drone works, like the manual control node of Ric Fehr (visit: https://github.com/ricfehr3/updated_testing_demos/tree/master/bebop_control/src).

Just open a new terminal, follow the creators instructions and execute it.

Its important to give the drones a distance of at least three and a half feet between them using the Y- axis of the world as reference.

As the master drone moves across the space the slave drone will try to replicate its movements using the position and orientation of the first one, keeping a distance between them.

## Bebop Color Follower

  - *Package:* **bebop_color_follower**
  - *Node:* **bebop_color_follower**

This program requires an object to be followed, it is able to track the object's color clicking on it at the camera window. Once selected and armed the Bebop drone will move to center its nose with the centroid of the object. Once the drone reach certain area of the object to calculate the distance between the drone and the object.


**This program helped the SKYLAG team to won the second place of the National Robotic Tournament 2018 in Mexico at the autonomus drone category, it was also considered be the best solution of the challenge.**
