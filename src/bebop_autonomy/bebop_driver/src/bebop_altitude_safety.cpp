//
// Created by NinadJadhav on 11/16/19.
//

#include <REACT/bebop_safety_checks.h>

float internal_altitude=0.0;
bool takeOff_permit = false;


void altCallback(const bebop_simple_test::Altitude::ConstPtr& msg){
        internal_altitude = msg->altitude;
}

void takeoff_callback(const std_msgs::Bool::ConstPtr& msg){
    takeOff_permit = msg->data;
}


BebopSafety::BebopSafety(ros::NodeHandle* nodeHandle): _nh(*nodeHandle){
    _nh.getParam("bebop_name", bebop_name);
    set_takeoff_pub = _nh.advertise<std_msgs::Bool>("/" + bebop_name + "/permit_takeoff",1);
    force_land_pub = _nh.advertise<std_msgs::Empty>("/"+ bebop_name +"/land",1);
    altitude_monitor_internal = _nh.subscribe("/" + bebop_name +"/states/ardrone3/PilotingState/AltitudeChanged",1000,altCallback);
    land_bebop = _nh.advertise<std_msgs::Empty>("/" + bebop_name +"/reset",1000);
    cmd_vel_pub_bebop = _nh.advertise<geometry_msgs::Twist>("/" + bebop_name +"/cmd_vel",1000);
    set_takeoff_sub = _nh.subscribe("/" + bebop_name  +"/permit_takeoff",1,takeoff_callback);
}


int main(int argc, char** argv){

    ros::init(argc,argv,"bebop_test", ros::init_options::AnonymousName);
    ros::NodeHandle n("~");
    float alt_soft_limit, alt_hard_limit;
    geometry_msgs::Twist cmd_vel_bebop;
    std_msgs::Bool tkof;
    tkof.data = false;
    int counter = 0;
    ros::Rate r(100);
    ros::Rate r2(200);
    bool monitor_flag = true;
    ros::Time start_takeoff;
    ros::Duration idle_duration(120);

    BebopSafety bebop_safety_check(&n);
    n.getParam("altitude_soft_limit", alt_soft_limit);
    n.getParam("altitude_hard_limit", alt_hard_limit);

    cmd_vel_bebop.linear.x = 0;
    cmd_vel_bebop.linear.y = 0;
    cmd_vel_bebop.linear.z = 0;
    cmd_vel_bebop.angular.x = 0;
    cmd_vel_bebop.angular.y = 0;
    cmd_vel_bebop.angular.z = 0;

    while(ros::ok()) {
        printf("[INFO] Enable TAKEOFF Permission!!\n");
        while (!takeOff_permit) {
            if(internal_altitude > 0) {
                //Kill the propellors
                bebop_safety_check.land_bebop.publish(bebop_safety_check.msg_land);
            }
            ros::spinOnce();
            r.sleep();
        }

        printf("[INFO] Permission To TakeOFF!!\n");
        monitor_flag = true;
        cmd_vel_bebop.linear.z = -1;
        start_takeoff = ros::Time::now();

        while (takeOff_permit) {
            //printf("Current altitude = %f \n", internal_altitude);
            if (internal_altitude >= alt_soft_limit) {

                printf("[CRITICAL] Exceeded Altitude Soft Limit Safety Boundary!!! \n");
                printf("[OVERRIDE] Resetting to Start position\n");

                while (internal_altitude >= 0.5) {
                    //Continouly publish negative velocity at maximum frequecy along z-axis to bring bebop as close to the ground as possible
                    printf("[CRITICAL Altitude] STOP ALL Controller Commands and Scripts \n");
                    bebop_safety_check.cmd_vel_pub_bebop.publish(cmd_vel_bebop);
                    bebop_safety_check.force_land_pub.publish(bebop_safety_check.msg_land);
                    r2.sleep();
                    ros::spinOnce();
                }

                monitor_flag = true;
                bebop_safety_check.set_takeoff_pub.publish(tkof);
            }

            if (internal_altitude >= alt_hard_limit) {
                printf("====== CRITICAL! Exceeded Height Hard Limit Safety Boundary!!!========\n");
            }

            ros::spinOnce();
            r.sleep(); //Use Rate only when monitoring.
            if (monitor_flag) {
                printf("[INFO] Monitoring altitude....\n");
                monitor_flag = false;
            }

            if(ros::Time::now() - start_takeoff  > idle_duration) {
                start_takeoff = ros::Time::now();
                if (internal_altitude <= 0.01) {
                    printf("[INFO] Bebop IDLE. Restricting TakeOFF\n");
                    bebop_safety_check.set_takeoff_pub.publish(tkof);
                }
            }

        }
    }
}
