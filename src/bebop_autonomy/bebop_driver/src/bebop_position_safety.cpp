//
// Created by NinadJadhav on 11/16/19.
//

#include <REACT/bebop_safety_checks.h>

bool boundary_limit_reached=false;
bool takeOff_permit = false;
bool force_land = false;
float z_pos = 0.0;
geometry_msgs::Twist current_cm5_vel_bebop;
float max_x = 3.5, max_y=2.0;

void takeoff_callback(const std_msgs::Bool::ConstPtr& msg){
    takeOff_permit = msg->data;
}

void positionCallbackMocap(const mocap_optitrack::PoseStampedCustom::ConstPtr& msg) {
    boundary_limit_reached = false;
    z_pos = msg->pose.position.z;
    if(msg->pose.position.x > max_x || msg->pose.position.y > max_y || msg->pose.position.x < -max_x || msg->pose.position.y < -max_y){
      boundary_limit_reached = true;
      //printf("%f , %f\n",msg->pose.position.x, msg->pose.position.y );
    }

}

// void cmdCallback(const geometry_msgs::Twist::ConstPtr& msg) {
//
// }



BebopSafety::BebopSafety(ros::NodeHandle* nodeHandle): _nh(*nodeHandle){
    _nh.getParam("bebop_name", bebop_name);
    cmd_vel_pub_bebop = _nh.advertise<geometry_msgs::Twist>("/" + bebop_name +"/cmd_vel",1000);
    set_takeoff_pub = _nh.advertise<std_msgs::Bool>("/" + bebop_name + "/permit_takeoff",1);
    force_land_pub = _nh.advertise<std_msgs::Empty>("/"+ bebop_name +"/land",1);
    //set_takeoff_sub = _nh.subscribe("/" + bebop_name  +"/permit_takeoff",1,takeoff_callback);
    bebop_sub = _nh.subscribe("/mocap_node/" + bebop_name  +"/pose",10,positionCallbackMocap);
    //cmd_vel_sub = _nh.subscribe("/" + bebop_name +"/cmd_vel",1000, cmdCallback);
}


int main(int argc, char** argv){

    ros::init(argc,argv,"bebop_pos_safety", ros::init_options::AnonymousName);
    ros::NodeHandle n("~");
    geometry_msgs::Twist cmd_vel_bebop;
    std_msgs::Bool tkof;
    tkof.data = false;
    ros::Rate r(300);
    bool printFlag = true;

    BebopSafety bebop_safety_check(&n);
    n.getParam("max_x", max_x);
    n.getParam("max_y", max_y);

    cmd_vel_bebop.linear.x = 0;
    cmd_vel_bebop.linear.y = 0;
    cmd_vel_bebop.linear.z = -1;
    cmd_vel_bebop.angular.x = 0;
    cmd_vel_bebop.angular.y = 0;
    cmd_vel_bebop.angular.z = 0;

    while(ros::ok()) {

          if (boundary_limit_reached) {

              printf("[CRITICAL] Exceeded Position Limit Safety Boundary!!! \n");
              printf("[OVERRIDE] Force Landing\n");

              while (z_pos >= 0.5) {
                  //Continouly publish negative velocity at maximum frequecy along z-axis to bring bebop as close to the ground as possible
                  printf("[CRITICAL Position] STOP ALL Controller Commands and Scripts \n");
                  bebop_safety_check.cmd_vel_pub_bebop.publish(cmd_vel_bebop);
                  bebop_safety_check.force_land_pub.publish(bebop_safety_check.msg_land);
                  r.sleep();
                  ros::spinOnce();
              }
              bebop_safety_check.set_takeoff_pub.publish(tkof);

              printFlag = true;
          }

          if(printFlag){
            printf("[INFO] Within safe boundary limits \n");
            printf("[INFO] Need take-off permission \n");
            printFlag = false;
          }
        ros::spinOnce();
        r.sleep();
    }
}
