//
// Created by NinadJadhav on 11/16/19.
//

#ifndef BEBOP_ROS_EXAMPLES_BEBOP_SAFETY_CHECKS_H
#define BEBOP_ROS_EXAMPLES_BEBOP_SAFETY_CHECKS_H

#include <ros/ros.h>
#include <std_msgs/Empty.h>
#include <std_msgs/UInt8.h>
#include <REACT/Altitude.h>
#include <REACT/ArcDrone.h>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Bool.h>
#include <stdio.h>

class BebopSafety{
    private:
    ros::Publisher set_takeoff_pub, force_land_pub;
    ros::Subscriber altitude_monitor_internal, altitude_monitor_mocap, set_takeoff_sub, force_land_sub;
    ros::Subscriber mocap_boundary;
    std::string bebop_name;
    ros::NodeHandle _nh;

    public:
    std_msgs::Empty msg_land;
    ros::Publisher land_bebop, cmd_vel_pub_bebop;
    BebopSafety(ros::NodeHandle* nodeHandle);
};


#endif //BEBOP_ROS_EXAMPLES_BEBOP_SAFETY_CHECKS_H
