#!/bin/bash

#this file should be inside catkin_ws/src
# This is a shell script which
# will perform roslaunching and
# passing in the coordinates
# command will look something like this: 
# ./bebop_launch_1.sh <x-coord> <y-coord>
ret_X="-5"
ret_Y="1"

# check how many parameters are passed...
if [ $# -eq 2 ]; then
	# cd into the flights directory of Bebop_waypoints
	DIRECTORY=Bebop_ROS_Examples/Bebop_waypoints/flights
	cd "$DIRECTORY"
	# check if the .bak file exists. If it doesn't 
	# create a backup
	FILE="test_1.yaml.bak"
	if [ ! -f "$FILE" ]; then
		mv test_1.yaml test_1.yaml.bak
	fi
	# compiling the yaml file
	gcc make_yaml_b.c -o make_yaml -lm
	# passing in coordinates to make a new yaml file
	./make_yaml_b $1 $2 $ret_X $ret_Y
	# cd ..
	# other parameters passed to ros before launching...
	roslaunch bebop_driver bebop_node.launch \
	ip:=192.168.1.18 namespace:=bebop_18 &
	DRIVER_PID=$!
	sleep 60

	rostopic pub --once /bebop_18/permit_takeoff std_msgs/Bool "data: true" &

	#roslaunch bebop_driver bebop_node.launch \
	#ip:=192.168.1.18 namespace:=bebop_18
	roslaunch Bebop_waypoints test_1.launch #&
	#WP_PID=$!

    	# kill $DRIVER_PID
	# sleep for 10 seconds before setting up a new test flight...
	echo "Landing for 10 seconds"
	sleep 10
	#kill WP_PID

	./make_yaml $ret_X $ret_Y $1 $2
	echo "Returning to base"

	rostopic pub --once /bebop_18/permit_takeoff std_msgs/Bool "data: true" &

	roslaunch Bebop_waypoints test_1.launch #&
	#WP_PID=$!
	kill $DRIVER_PID
	echo "Base landing point reached"
fi
