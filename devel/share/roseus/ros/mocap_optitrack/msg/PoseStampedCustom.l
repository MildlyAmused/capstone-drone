;; Auto-generated. Do not edit!


(when (boundp 'mocap_optitrack::PoseStampedCustom)
  (if (not (find-package "MOCAP_OPTITRACK"))
    (make-package "MOCAP_OPTITRACK"))
  (shadow 'PoseStampedCustom (find-package "MOCAP_OPTITRACK")))
(unless (find-package "MOCAP_OPTITRACK::POSESTAMPEDCUSTOM")
  (make-package "MOCAP_OPTITRACK::POSESTAMPEDCUSTOM"))

(in-package "ROS")
;;//! \htmlinclude PoseStampedCustom.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass mocap_optitrack::PoseStampedCustom
  :super ros::object
  :slots (_header _pose _latency _true_nsec ))

(defmethod mocap_optitrack::PoseStampedCustom
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:pose __pose) (instance geometry_msgs::Pose :init))
    ((:latency __latency) 0)
    ((:true_nsec __true_nsec) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _pose __pose)
   (setq _latency (round __latency))
   (setq _true_nsec (round __true_nsec))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:pose
   (&rest __pose)
   (if (keywordp (car __pose))
       (send* _pose __pose)
     (progn
       (if __pose (setq _pose (car __pose)))
       _pose)))
  (:latency
   (&optional __latency)
   (if __latency (setq _latency __latency)) _latency)
  (:true_nsec
   (&optional __true_nsec)
   (if __true_nsec (setq _true_nsec __true_nsec)) _true_nsec)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; geometry_msgs/Pose _pose
    (send _pose :serialization-length)
    ;; uint64 _latency
    8
    ;; uint64 _true_nsec
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; geometry_msgs/Pose _pose
       (send _pose :serialize s)
     ;; uint64 _latency
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _latency (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _latency) (= (length (_latency . bv)) 2)) ;; bignum
              (write-long (ash (elt (_latency . bv) 0) 0) s)
              (write-long (ash (elt (_latency . bv) 1) -1) s))
             ((and (class _latency) (= (length (_latency . bv)) 1)) ;; big1
              (write-long (elt (_latency . bv) 0) s)
              (write-long (if (>= _latency 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _latency s)(write-long (if (>= _latency 0) 0 #xffffffff) s)))
     ;; uint64 _true_nsec
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _true_nsec (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _true_nsec) (= (length (_true_nsec . bv)) 2)) ;; bignum
              (write-long (ash (elt (_true_nsec . bv) 0) 0) s)
              (write-long (ash (elt (_true_nsec . bv) 1) -1) s))
             ((and (class _true_nsec) (= (length (_true_nsec . bv)) 1)) ;; big1
              (write-long (elt (_true_nsec . bv) 0) s)
              (write-long (if (>= _true_nsec 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _true_nsec s)(write-long (if (>= _true_nsec 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; geometry_msgs/Pose _pose
     (send _pose :deserialize buf ptr-) (incf ptr- (send _pose :serialization-length))
   ;; uint64 _latency
     
#+(or :alpha :irix6 :x86_64)
      (setf _latency (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _latency (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; uint64 _true_nsec
     
#+(or :alpha :irix6 :x86_64)
      (setf _true_nsec (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _true_nsec (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(setf (get mocap_optitrack::PoseStampedCustom :md5sum-) "da268b13f28e8966efc5405127bf83b8")
(setf (get mocap_optitrack::PoseStampedCustom :datatype-) "mocap_optitrack/PoseStampedCustom")
(setf (get mocap_optitrack::PoseStampedCustom :definition-)
      "std_msgs/Header header
geometry_msgs/Pose pose
uint64 latency
uint64 true_nsec

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: geometry_msgs/Pose
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: geometry_msgs/Quaternion
# This represents an orientation in free space in quaternion form.

float64 x
float64 y
float64 z
float64 w

")



(provide :mocap_optitrack/PoseStampedCustom "da268b13f28e8966efc5405127bf83b8")


