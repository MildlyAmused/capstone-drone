;; Auto-generated. Do not edit!


(when (boundp 'Bebop_waypoints::Waypoints)
  (if (not (find-package "BEBOP_WAYPOINTS"))
    (make-package "BEBOP_WAYPOINTS"))
  (shadow 'Waypoints (find-package "BEBOP_WAYPOINTS")))
(unless (find-package "BEBOP_WAYPOINTS::WAYPOINTS")
  (make-package "BEBOP_WAYPOINTS::WAYPOINTS"))

(in-package "ROS")
;;//! \htmlinclude Waypoints.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass Bebop_waypoints::Waypoints
  :super ros::object
  :slots (_header _wps ))

(defmethod Bebop_waypoints::Waypoints
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:wps __wps) (let (r) (dotimes (i 0) (push (instance Bebop_waypoints::Waypoint :init) r)) r))
    )
   (send-super :init)
   (setq _header __header)
   (setq _wps __wps)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:wps
   (&rest __wps)
   (if (keywordp (car __wps))
       (send* _wps __wps)
     (progn
       (if __wps (setq _wps (car __wps)))
       _wps)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; Bebop_waypoints/Waypoint[] _wps
    (apply #'+ (send-all _wps :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; Bebop_waypoints/Waypoint[] _wps
     (write-long (length _wps) s)
     (dolist (elem _wps)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; Bebop_waypoints/Waypoint[] _wps
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _wps (let (r) (dotimes (i n) (push (instance Bebop_waypoints::Waypoint :init) r)) r))
     (dolist (elem- _wps)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get Bebop_waypoints::Waypoints :md5sum-) "ea267265f0ce23247f42c63f13d835af")
(setf (get Bebop_waypoints::Waypoints :datatype-) "Bebop_waypoints/Waypoints")
(setf (get Bebop_waypoints::Waypoints :definition-)
      "Header header
Waypoint[] wps

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: Bebop_waypoints/Waypoint
Header header
geometry_msgs/Point wp
float64 hdg

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

")



(provide :Bebop_waypoints/Waypoints "ea267265f0ce23247f42c63f13d835af")


