;; Auto-generated. Do not edit!


(when (boundp 'Bebop_waypoints::Waypoint)
  (if (not (find-package "BEBOP_WAYPOINTS"))
    (make-package "BEBOP_WAYPOINTS"))
  (shadow 'Waypoint (find-package "BEBOP_WAYPOINTS")))
(unless (find-package "BEBOP_WAYPOINTS::WAYPOINT")
  (make-package "BEBOP_WAYPOINTS::WAYPOINT"))

(in-package "ROS")
;;//! \htmlinclude Waypoint.msg.html
(if (not (find-package "GEOMETRY_MSGS"))
  (ros::roseus-add-msgs "geometry_msgs"))
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass Bebop_waypoints::Waypoint
  :super ros::object
  :slots (_header _wp _hdg ))

(defmethod Bebop_waypoints::Waypoint
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:wp __wp) (instance geometry_msgs::Point :init))
    ((:hdg __hdg) 0.0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _wp __wp)
   (setq _hdg (float __hdg))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:wp
   (&rest __wp)
   (if (keywordp (car __wp))
       (send* _wp __wp)
     (progn
       (if __wp (setq _wp (car __wp)))
       _wp)))
  (:hdg
   (&optional __hdg)
   (if __hdg (setq _hdg __hdg)) _hdg)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; geometry_msgs/Point _wp
    (send _wp :serialization-length)
    ;; float64 _hdg
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; geometry_msgs/Point _wp
       (send _wp :serialize s)
     ;; float64 _hdg
       (sys::poke _hdg (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; geometry_msgs/Point _wp
     (send _wp :deserialize buf ptr-) (incf ptr- (send _wp :serialization-length))
   ;; float64 _hdg
     (setq _hdg (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get Bebop_waypoints::Waypoint :md5sum-) "ad3f8bb2ece6e08f9216afcf0bca3657")
(setf (get Bebop_waypoints::Waypoint :datatype-) "Bebop_waypoints/Waypoint")
(setf (get Bebop_waypoints::Waypoint :definition-)
      "Header header
geometry_msgs/Point wp
float64 hdg

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

")



(provide :Bebop_waypoints/Waypoint "ad3f8bb2ece6e08f9216afcf0bca3657")


