; Auto-generated. Do not edit!


(cl:in-package datalogger-msg)


;//! \htmlinclude Altitude_datalogger.msg.html

(cl:defclass <Altitude_datalogger> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (altitude
    :reader altitude
    :initarg :altitude
    :type cl:float
    :initform 0.0))
)

(cl:defclass Altitude_datalogger (<Altitude_datalogger>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Altitude_datalogger>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Altitude_datalogger)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name datalogger-msg:<Altitude_datalogger> is deprecated: use datalogger-msg:Altitude_datalogger instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <Altitude_datalogger>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader datalogger-msg:header-val is deprecated.  Use datalogger-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'altitude-val :lambda-list '(m))
(cl:defmethod altitude-val ((m <Altitude_datalogger>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader datalogger-msg:altitude-val is deprecated.  Use datalogger-msg:altitude instead.")
  (altitude m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Altitude_datalogger>) ostream)
  "Serializes a message object of type '<Altitude_datalogger>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((bits (roslisp-utils:encode-double-float-bits (cl:slot-value msg 'altitude))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Altitude_datalogger>) istream)
  "Deserializes a message object of type '<Altitude_datalogger>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'altitude) (roslisp-utils:decode-double-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Altitude_datalogger>)))
  "Returns string type for a message object of type '<Altitude_datalogger>"
  "datalogger/Altitude_datalogger")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Altitude_datalogger)))
  "Returns string type for a message object of type 'Altitude_datalogger"
  "datalogger/Altitude_datalogger")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Altitude_datalogger>)))
  "Returns md5sum for a message object of type '<Altitude_datalogger>"
  "5073f650d09c8192d358641b48a0204b")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Altitude_datalogger)))
  "Returns md5sum for a message object of type 'Altitude_datalogger"
  "5073f650d09c8192d358641b48a0204b")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Altitude_datalogger>)))
  "Returns full string definition for message of type '<Altitude_datalogger>"
  (cl:format cl:nil "# Ardrone3PilotingStateAltitudeChanged~%# auto-generated from up stream XML files at~%#   github.com/Parrot-Developers/libARCommands/tree/master/Xml~%# To check upstream commit hash, refer to last_build_info file~%# Do not modify this file by hand. Check scripts/meta folder for generator files.~%#~%# SDK Comment: Drones altitude changed.\\n The altitude reported is the altitude above the take off point.\\n To get the altitude above sea level, see [PositionChanged](#1-4-4).~%~%Header header~%~%# Altitude in meters~%float64 altitude~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Altitude_datalogger)))
  "Returns full string definition for message of type 'Altitude_datalogger"
  (cl:format cl:nil "# Ardrone3PilotingStateAltitudeChanged~%# auto-generated from up stream XML files at~%#   github.com/Parrot-Developers/libARCommands/tree/master/Xml~%# To check upstream commit hash, refer to last_build_info file~%# Do not modify this file by hand. Check scripts/meta folder for generator files.~%#~%# SDK Comment: Drones altitude changed.\\n The altitude reported is the altitude above the take off point.\\n To get the altitude above sea level, see [PositionChanged](#1-4-4).~%~%Header header~%~%# Altitude in meters~%float64 altitude~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Altitude_datalogger>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Altitude_datalogger>))
  "Converts a ROS message object to a list"
  (cl:list 'Altitude_datalogger
    (cl:cons ':header (header msg))
    (cl:cons ':altitude (altitude msg))
))
