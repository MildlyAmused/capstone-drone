
(cl:in-package :asdf)

(defsystem "datalogger-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "Altitude_datalogger" :depends-on ("_package_Altitude_datalogger"))
    (:file "_package_Altitude_datalogger" :depends-on ("_package"))
  ))