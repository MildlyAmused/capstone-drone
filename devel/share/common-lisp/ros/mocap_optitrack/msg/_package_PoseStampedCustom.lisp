(cl:in-package mocap_optitrack-msg)
(cl:export '(HEADER-VAL
          HEADER
          POSE-VAL
          POSE
          LATENCY-VAL
          LATENCY
          TRUE_NSEC-VAL
          TRUE_NSEC
))