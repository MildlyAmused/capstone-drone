
(cl:in-package :asdf)

(defsystem "mocap_optitrack-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :geometry_msgs-msg
               :std_msgs-msg
)
  :components ((:file "_package")
    (:file "PoseStampedCustom" :depends-on ("_package_PoseStampedCustom"))
    (:file "_package_PoseStampedCustom" :depends-on ("_package"))
  ))