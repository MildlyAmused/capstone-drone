; Auto-generated. Do not edit!


(cl:in-package mocap_optitrack-msg)


;//! \htmlinclude PoseStampedCustom.msg.html

(cl:defclass <PoseStampedCustom> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (pose
    :reader pose
    :initarg :pose
    :type geometry_msgs-msg:Pose
    :initform (cl:make-instance 'geometry_msgs-msg:Pose))
   (latency
    :reader latency
    :initarg :latency
    :type cl:integer
    :initform 0)
   (true_nsec
    :reader true_nsec
    :initarg :true_nsec
    :type cl:integer
    :initform 0))
)

(cl:defclass PoseStampedCustom (<PoseStampedCustom>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <PoseStampedCustom>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'PoseStampedCustom)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name mocap_optitrack-msg:<PoseStampedCustom> is deprecated: use mocap_optitrack-msg:PoseStampedCustom instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <PoseStampedCustom>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader mocap_optitrack-msg:header-val is deprecated.  Use mocap_optitrack-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'pose-val :lambda-list '(m))
(cl:defmethod pose-val ((m <PoseStampedCustom>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader mocap_optitrack-msg:pose-val is deprecated.  Use mocap_optitrack-msg:pose instead.")
  (pose m))

(cl:ensure-generic-function 'latency-val :lambda-list '(m))
(cl:defmethod latency-val ((m <PoseStampedCustom>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader mocap_optitrack-msg:latency-val is deprecated.  Use mocap_optitrack-msg:latency instead.")
  (latency m))

(cl:ensure-generic-function 'true_nsec-val :lambda-list '(m))
(cl:defmethod true_nsec-val ((m <PoseStampedCustom>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader mocap_optitrack-msg:true_nsec-val is deprecated.  Use mocap_optitrack-msg:true_nsec instead.")
  (true_nsec m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <PoseStampedCustom>) ostream)
  "Serializes a message object of type '<PoseStampedCustom>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'pose) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'latency)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'latency)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'latency)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'latency)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'latency)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'latency)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'latency)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'latency)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'true_nsec)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'true_nsec)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'true_nsec)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'true_nsec)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'true_nsec)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'true_nsec)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'true_nsec)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'true_nsec)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <PoseStampedCustom>) istream)
  "Deserializes a message object of type '<PoseStampedCustom>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'pose) istream)
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'latency)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'latency)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'latency)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'latency)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'latency)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'latency)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'latency)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'latency)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 0) (cl:slot-value msg 'true_nsec)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) (cl:slot-value msg 'true_nsec)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) (cl:slot-value msg 'true_nsec)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) (cl:slot-value msg 'true_nsec)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 32) (cl:slot-value msg 'true_nsec)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 40) (cl:slot-value msg 'true_nsec)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 48) (cl:slot-value msg 'true_nsec)) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 56) (cl:slot-value msg 'true_nsec)) (cl:read-byte istream))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<PoseStampedCustom>)))
  "Returns string type for a message object of type '<PoseStampedCustom>"
  "mocap_optitrack/PoseStampedCustom")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'PoseStampedCustom)))
  "Returns string type for a message object of type 'PoseStampedCustom"
  "mocap_optitrack/PoseStampedCustom")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<PoseStampedCustom>)))
  "Returns md5sum for a message object of type '<PoseStampedCustom>"
  "da268b13f28e8966efc5405127bf83b8")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'PoseStampedCustom)))
  "Returns md5sum for a message object of type 'PoseStampedCustom"
  "da268b13f28e8966efc5405127bf83b8")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<PoseStampedCustom>)))
  "Returns full string definition for message of type '<PoseStampedCustom>"
  (cl:format cl:nil "std_msgs/Header header~%geometry_msgs/Pose pose~%uint64 latency~%uint64 true_nsec~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'PoseStampedCustom)))
  "Returns full string definition for message of type 'PoseStampedCustom"
  (cl:format cl:nil "std_msgs/Header header~%geometry_msgs/Pose pose~%uint64 latency~%uint64 true_nsec~%~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%================================================================================~%MSG: geometry_msgs/Pose~%# A representation of pose in free space, composed of position and orientation. ~%Point position~%Quaternion orientation~%~%================================================================================~%MSG: geometry_msgs/Point~%# This contains the position of a point in free space~%float64 x~%float64 y~%float64 z~%~%================================================================================~%MSG: geometry_msgs/Quaternion~%# This represents an orientation in free space in quaternion form.~%~%float64 x~%float64 y~%float64 z~%float64 w~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <PoseStampedCustom>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'pose))
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <PoseStampedCustom>))
  "Converts a ROS message object to a list"
  (cl:list 'PoseStampedCustom
    (cl:cons ':header (header msg))
    (cl:cons ':pose (pose msg))
    (cl:cons ':latency (latency msg))
    (cl:cons ':true_nsec (true_nsec msg))
))
