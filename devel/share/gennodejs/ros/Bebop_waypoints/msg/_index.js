
"use strict";

let Waypoints = require('./Waypoints.js');
let Waypoint = require('./Waypoint.js');

module.exports = {
  Waypoints: Waypoints,
  Waypoint: Waypoint,
};
